/* window.vala
 *
 * Copyright 2020 Patrick Zanon
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace ExampleWebkit {
	using WebKit;

	/*
	 * [GtkTemplate (ui=...)]
	 * Documentation about Vala code attributes, see:
	 *   https://wiki.gnome.org/Projects/Vala/Manual/Attributes#Gtk_attributes
	 * 
	 * Can only be applied to classes that inherit from Gtk.Widget. 
	 * The "ui" argument is mandatory, and specifies the .ui gresource to be used 
	 * for building the Gtk widget
	 */
	[GtkTemplate (ui = "/org/gnome/Example-Webkit/window.ui")]
	public class Window : Gtk.ApplicationWindow {
		/*
		 * [GtkChild [name=..., internal=...]]
		 * Documentation about Vala code attributes, see:
		 *   https://wiki.gnome.org/Projects/Vala/Manual/Attributes#Gtk_attributes
		 * 
		 * Can only be applied to fields of classes being marked with [GtkTemplate]. 
		 * It's used to connect a field with a child object in the Gtk builder definition.
		 * 
		 * The optional "name" specifies a custom name being used in the Gtk builder 
		 * ui definition. By default the name of the marked field is used.
		 * 
		 * The optional "internal" specifies whether this child is internal or not in 
		 * the Gtk builder ui definition.
		 */
		//[GtkChild]
		//Gtk.Box boxContainer;
		//[GtkChild]
		//Gtk.Label label;
		//[GtkChild]
		//Gtk.ScrolledWindow scrolledWindow;
		//[GtkChild]
		//Gtk.Viewport viewPort;
		[GtkChild]
		WebKit.WebView webKitView;

		public Window (Gtk.Application app) {
			/*
			 * Workaround for the WebKit bug, see:
			 *  
			 *  + https://bugs.webkit.org/show_bug.cgi?id=175937
			 *  + https://stackoverflow.com/questions/60126579/gtk-builder-error-quark-invalid-object-type-webkitwebview
			 * 
			 * To solve the problem of Gtk.Builder with WebKit, which doesn't know about 
			 * WebKit.WebView type, a temporary variable 'wa' is created, so that the
			 * WebKit type is loaded from the system before calling Gtk.Builder
			 */ 
			var wa = new WebKit.WebView();
			wa = null;
		    
			//
			// Gtk:Builder call
			// 
			Object (application: app);

			//
			// WebKit.WebView call
			//
			webKitView.load_uri("https://www.gnome.org");
		}
	}
}
